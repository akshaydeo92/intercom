package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	fmt.Print("Enter file path: ")
	reader := bufio.NewReader(os.Stdin)

	input, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("An error occured while reading input. Please try again", err)
		return
	}

	input = strings.TrimSuffix(input, "\n")
	file, err := ioutil.ReadFile(input)
	if err != nil {
		panic(err)
	}

	err = getNearByUsers(file, 100)
	if err != nil {
		panic(err)
	}
}
