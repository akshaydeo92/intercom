package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

type UserMetaData struct {
	ID        int    `json:"user_id"`
	Name      string `json:"name"`
	Latitude  string `json:"latitude"`
	Longitude string `json:"longitude"`
}

func getNearByUsers(userData []byte, distance int) error {
	serializedUserData, err := serializeUserData(userData)
	if err != nil {
		return err
	}

	usersWithinDistance, err := getUsersWithinDistance(serializedUserData, distance)
	if err != nil {
		return err
	}
	if len(usersWithinDistance) == 0 {
		return errors.New(fmt.Sprintf("No users within given distance %d kms found", distance))
	}

	sort.Slice(usersWithinDistance, func(i, j int) bool {
		return usersWithinDistance[i].ID < usersWithinDistance[j].ID
	})

	fmt.Println("The following users live within the given distance:")
	for _, user := range usersWithinDistance {
		fmt.Printf("UserID:%d,Name:%s\n", user.ID, user.Name)
	}
	return nil
}

func serializeUserData(userData []byte) ([]UserMetaData, error) {
	result := []UserMetaData{}
	stringUsers := strings.Split(string(userData), "\n")

	for i, user := range stringUsers {
		userMD := UserMetaData{}
		err := json.Unmarshal([]byte(user), &userMD)
		if err != nil {
			return nil, errors.New(fmt.Sprintf("invalid file format, line number:%d", i))
		}
		result = append(result, userMD)
	}
	return result, nil
}

func getUsersWithinDistance(users []UserMetaData, distance int) ([]UserMetaData, error) {
	result := []UserMetaData{}

	for _, user := range users {
		latitude, err := strconv.ParseFloat(user.Latitude, 64)
		if err != nil {
			return nil, err
		}
		longitude, err := strconv.ParseFloat(user.Longitude, 64)
		if err != nil {
			return nil, err
		}

		distance := calculateDistanceFromCoordinates(latitude, longitude)
		if int(distance) <= 100 {
			u := UserMetaData{
				ID:   user.ID,
				Name: user.Name,
			}
			result = append(result, u)
		}
	}
	return result, nil
}
