package main

import (
	"testing"
)

func Test_serializeUserData(t *testing.T) {

	t.Run("happy path", func(t *testing.T) {
		buf := []byte(`{"latitude": "51", "user_id": 1, "name": "ABCD", "longitude": "-6"}
{"latitude": "51", "user_id": 2, "name": "EFGH", "longitude": "-9"}`)
		expected := []UserMetaData{
			{
				ID:        1,
				Name:      "ABCD",
				Latitude:  "51",
				Longitude: "-6",
			},
			{
				ID:        2,
				Name:      "EFGH",
				Latitude:  "51",
				Longitude: "-9",
			},
		}
		actual, err := serializeUserData(buf)
		if err == nil {
			if len(actual) == len(expected) {
				for i := range actual {
					if actual[i] != expected[i] {
						t.Errorf("acutal and expected do not match :%s", err)
					}
				}
			}

		} else {
			t.Errorf("acutal and expected do not match:%s", err)
		}
	})

	t.Run("sad path", func(t *testing.T) {
		buf := []byte(`{"latitude": "51", "user_id": "1", "name": "ABCD", "longitude": "-6"}
{"latitude": "51", "user_id": 2, "name": "EFGH", "longitude": "-9"}`)
		_, err := serializeUserData(buf)
		if err == nil {
			t.Error("expected invalid formatting error")
		}
	})
}

func Test_getUsersWithinDistance(t *testing.T) {
	t.Run("happy path", func(t *testing.T) {
		users := []UserMetaData{
			{
				ID:        1,
				Name:      "ABCD",
				Latitude:  "52.986375",
				Longitude: "-6",
			},
			{
				ID:        2,
				Name:      "EFGH",
				Latitude:  "52.986375",
				Longitude: "-9",
			},
			{
				ID:        3,
				Name:      "IJKL",
				Latitude:  "52.986375",
				Longitude: "-7",
			},
		}
		expected := []UserMetaData{
			{
				ID:   1,
				Name: "ABCD",
			},
			{
				ID:   3,
				Name: "IJKL",
			},
		}
		actual, err := getUsersWithinDistance(users, 100)
		if err == nil {
			if len(actual) == len(expected) {
				for i := range actual {
					if actual[i] != expected[i] {
						t.Errorf("acutal and expected do not match")
					}
				}
			}

		} else {
			t.Errorf("acutal and expected do not match:%s", err)
		}
	})
	t.Run("sad path", func(t *testing.T) {
		users := []UserMetaData{
			{
				ID:        1,
				Name:      "ABCD",
				Latitude:  "52.9as86375",
				Longitude: "-6",
			},
			{
				ID:        2,
				Name:      "EFGH",
				Latitude:  "52.986375",
				Longitude: "-9",
			},
			{
				ID:        3,
				Name:      "IJKL",
				Latitude:  "52.9d86375",
				Longitude: "-7",
			},
		}
		_, err := getUsersWithinDistance(users, 100)
		if err == nil {
			t.Error("expected invalid formatting error")

		}
	})
}
