package main

import "math"

const (
	radiousOfEarth       = 6335.439
	homeLatitudeDegrees  = 53.339428
	homeLongitudeDegrees = -6.257664
)

var (
	homeLatitudeRadian  = degreeToRadians(homeLatitudeDegrees)
	homeLongitudeRadian = degreeToRadians(homeLongitudeDegrees)
)

func degreeToRadians(degree float64) float64 {
	return degree * (math.Pi / 180.0)
}

func haverSine(theta float64) float64 {
	return math.Pow(math.Sin(theta/2), 2)
}

// We are using the Haversine formula
// see https://en.wikipedia.org/wiki/Great-circle_distance and
// https://en.wikipedia.org/wiki/Haversine_formula
func calculateDistanceFromCoordinates(destinationLatitude, destinationLongitude float64) float64 {
	destinationLatitudeRadian := degreeToRadians(destinationLatitude)
	destinationLongitudeRadian := degreeToRadians(destinationLongitude)

	centralAngleP1 := haverSine(math.Abs(destinationLatitudeRadian - homeLatitudeRadian))
	centralAngleP2 := math.Cos(homeLatitudeRadian) * math.Cos(destinationLatitudeRadian) * haverSine(math.Abs(destinationLongitudeRadian-homeLongitudeRadian))
	centralAngle := centralAngleP1 + centralAngleP2

	return 2 * radiousOfEarth * math.Asin(math.Sqrt(centralAngle))
}
