This repository contains the solution for the take home test challenge for Intercom.

This file contains the details about the steps needed to run the program.

* Install go on your local machine, you can follow the instructions here https://golang.org/doc/install
 * If you have a Mac and have brew installed a simple `brew install go` would work

* Once you download the repository set your GOPATH
 * go env -w GOPATH=<local-path>/intercom

* Once you go in the intercom folder run `go build main.go formulae.go users.go`
 * Then run ./main for the tool to run
 * Enter path for file and then hit enter to get the output 
 * To check test you can simply run `go test`


I am attaching the output in output.txt as well