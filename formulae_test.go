package main

import "testing"

type testDegreeToRadian struct {
	arg1, expected float64
}

func Test_degreeToRadians(t *testing.T) {
	tests := []testDegreeToRadian{
		{52.986375, 0.9247867024464105},
		{51.92893, 0.906330805537659},
		{53, 0.9250245035569946},
	}
	for _, test := range tests {
		if output := degreeToRadians(test.arg1); output != test.expected {
			t.Errorf("Output %v not equal to expected %v", output, test.expected)
		}

	}
}

type testdistanceFromCoordinates struct {
	arg1, arg2, expected float64
}

func Test_calculateDistanceFromRadianCoordinates(t *testing.T) {

	tests := []testdistanceFromCoordinates{
		{52.986375, -6.043701, 41.53558507585811},
		{51.92893, -10.27699, 311.50713533644637},
		{53, -7, 61.88434367492471},
	}
	for _, test := range tests {
		if output := calculateDistanceFromCoordinates(test.arg1, test.arg2); output != test.expected {
			t.Errorf("Output %v not equal to expected %v", output, test.expected)
		}

	}
}
